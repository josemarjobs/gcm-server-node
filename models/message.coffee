class Message
	constructor: ->
		@collapseKey = null
		@timeToLive = null
		@delayWhileIdle = null
		@dryRun = null
		@restrictedPackageName = null
		@data = null
		@dataCount = 0

	addData: (key, value) ->
		@data = {} if @data is null
		@dataCount += 1
		@data[key] = value

	toString: ->
		message = "Message("
		message += "collapseKey=#{@collapseKey}," if @collapseKey isnt null
		message += "timeToLive=#{@timeToLive}," if @timeToLive isnt null
		message += "delayWhileIdle=#{@delayWhileIdle}," if @delayWhileIdle isnt null
		message += "dryRun=#{@dryRun}," if @dryRun isnt null
		message += "restrictedPackageName=#{@restrictedPackageName}," if @restrictedPackageName isnt null

		d = @generateData()
		message += d if @dataCount > 0

		message = @removeLastComma message
		
		message += ")"
		message

	generateData: ->
		d = "data:{"
		d += "#{key}=#{@data[key]}," for key of @data
		d = @removeLastComma d
		d += "}"
		d

	removeLastComma: (message) ->
		if message[message.length - 1] is ","
			message = message.substring(0, message.length - 1)
		message

module.exports = Message