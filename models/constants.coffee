Constants = 
	# Endpoint for sending messages.
	GCM_SEND_ENDPOINT: "https://android.googleapis.com/gcm/send"

	# HTTP parameter for registration id.
	PARAM_REGISTRATION_ID: "registration_id"

	# HTTP parameter for collapse key.
	PARAM_COLLAPSE_KEY: "collapse_key"

	# HTTP parameter for delaying the message delivery if the device is idle.
	PARAM_DELAY_WHILE_IDLE: "delay_while_idle"

	# HTTP parameter for telling gcm to validate the message without actually sending it.
	PARAM_DRY_RUN: "dry_run"

	# HTTP parameter for package name that can be used to restrict message delivery by matching
	# against the package name used to generate the registration id.
	PARAM_RESTRICTED_PACKAGE_NAME: "restricted_package_name"

	# Prefix to HTTP parameter used to pass key-values in the message payload.
	PARAM_PAYLOAD_PREFIX: "data."

	# Prefix to HTTP parameter used to set the message time-to-live.
	PARAM_TIME_TO_LIVE: "time_to_live"

	# Too many messages sent by the sender. Retry after a while.
	ERROR_QUOTA_EXCEEDED: "QuotaExceeded"

	# Too many messages sent by the sender to a specific device.
	# Retry after a while.
	ERROR_DEVICE_QUOTA_EXCEEDED: "DeviceQuotaExceeded"

	# Missing registration_id.
	# Sender should always add the registration_id to the request.
	ERROR_MISSING_REGISTRATION: "MissingRegistration"

	# Bad registration_id. Sender should remove this registration_id.
	ERROR_INVALID_REGISTRATION: "InvalidRegistration"

	# The sender_id contained in the registration_id does not match the
	# sender_id used to register with the GCM servers.
	ERROR_MISMATCH_SENDER_ID: "MismatchSenderId"

	# The user has uninstalled the application or turned off notifications.
	# Sender should stop sending messages to this device and delete the
	# registration_id. The client needs to re-register with the GCM servers to
	# receive notifications again.
	ERROR_NOT_REGISTERED: "NotRegistered"

	# The payload of the message is too big, see the limitations.
	# Reduce the size of the message.
	ERROR_MESSAGE_TOO_BIG: "MessageTooBig"

	# Collapse key is required. Include collapse key in the request.
	ERROR_MISSING_COLLAPSE_KEY: "MissingCollapseKey"

	# A particular message could not be sent because the GCM servers were not
	# available. Used only on JSON requests, as in plain text requests
	# unavailability is indicated by a 503 response.
	ERROR_UNAVAILABLE: "Unavailable"

	# A particular message could not be sent because the GCM servers encountered
	# an error. Used only on JSON requests, as in plain text requests internal
	# errors are indicated by a 500 response.
	ERROR_INTERNAL_SERVER_ERROR: "InternalServerError"

	# Time to Live value passed is less than zero or more than maximum.
	ERROR_INVALID_TTL: "InvalidTtl"

	# Token returned by GCM when a message was successfully sent.
	TOKEN_MESSAGE_ID: "id"

	# Token returned by GCM when the requested registration id has a canonical
	# value.
	TOKEN_CANONICAL_REG_ID: "registration_id"

	# Token returned by GCM when there was an error sending a message.
	TOKEN_ERROR: "Error"

	# JSON-only field representing the registration ids.
	JSON_REGISTRATION_IDS: "registration_ids"

	# JSON-only field representing the payload data.
	JSON_PAYLOAD: "data"

	# JSON-only field representing the number of successful messages.
	JSON_SUCCESS: "success"

	# JSON-only field representing the number of failed messages.
	JSON_FAILURE: "failure"

	# JSON-only field representing the number of messages with a canonical
	# registration id.
	JSON_CANONICAL_IDS: "canonical_ids"

	# JSON-only field representing the id of the multicast request.
	JSON_MULTICAST_ID: "multicast_id"

	# JSON-only field representing the result of each individual request.
	JSON_RESULTS: "results"

	# JSON-only field representing the error field of an individual request.
	JSON_ERROR: "error"

	# JSON-only field sent by GCM when a message was successfully sent.
	JSON_MESSAGE_ID: "message_id"

module.exports = Constants
