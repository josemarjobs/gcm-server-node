class Result
	constructor: ->
		@messageId = null
		@canonicalRegistrationId = null
		@errorCode = null

	toString: ->
		result = "["
		result += " messageId=#{@messageId}" if @messageId isnt null
		result += " canonicalRegistrationId=#{@canonicalRegistrationId}" if @canonicalRegistrationId isnt null
		result += " errorCode=#{@errorCode}" if @errorCode isnt null
		result += " ]"
		result

module.exports = Result