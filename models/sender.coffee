util = require 'util'
Constants = require './Constants'
Result = require './result'

class Sender
	@UTF8 = "UTF-8"
	# Initial delay before first retry, without jitter.
	@BACKOFF_INITIAL_DELAY = 1000

	# Maximum delay before a retry.
	@MAX_BACKOFF_DELAY = 1024000

	@Random = Math.random

	constructor: (key) ->
    @key = @nonNull(key)
    @body = ""

	nonNull: (argument = null) ->
    if argument is null
      throw new Error("argument cannot be null")
    argument

  # Creates a map with just one key-value pair
  newKeyValues: (key, value) ->
    {"#{@nonNull(key)}":"#{@nonNull(value)}"}

  # Creates a string to be used as the body
  newBody: (name, value) ->
    @body = "#{@nonNull(name)}=#{@nonNull(value)}"

  # Adds a new parameter to the http POST body
  addParameter: (body, name, value) ->
    @body += "#{@nonNull(body)}&#{@nonNull(name)}=#{@nonNull(value)}"

  send: (message, registrationId, retries) ->
  	attempt = 0
  	result = null
  	backoff = BACKOFF_INITIAL_DELAY
  	tryAgain = true
  	while tryAgain
  		attempt += 1
  		util.log "Attempt ##{attempt} to send message #{message.toString()} to regIds " + registrationId
  		result = @sendNoRetry(message, registrationId)
  		tryAgain = result is null and attempt <= retries

  	if result is null
  		throw new Error("Could not send message after #{attempt} attempts")

  	result

  sendNoRetry: (message, registrationId) ->
    if message.delayWhileIdle
      console.log message.delayWhileIdle
    
  	new Result()

module.exports = Sender