chai = sys = require 'chai'
assert = chai.assert
Sender = require '../models/sender'
Result = require '../models/result'


describe 'Sender', ->
	beforeEach ->
		@sender = new Sender(1234)

	it 'creates a new key-value pair', ->
		kv = @sender.newKeyValues 'key', 'value'
		assert.deepEqual {'key':'value'}, kv

	it 'creates a new body', ->
		@sender.newBody "name", "value"
		assert.equal "name=value", @sender.body

	it 'adds parameter to body', ->
		@sender.addParameter "body", "name", "value"
		assert.equal "body&name=value", @sender.body

	describe '#nonNull', ->
		it 'throws error when Argument is null', ->
			try
			 @sender.nonNull null
			 assert.fail "Argument must not be null"
			catch Error
		it 'does not throws error when Argument is not null', ->
			try
			 @sender.nonNull '123'
			catch Error
			 assert.fail "Argument must not be null"

			 



