assert = require 'assert'
Message = require '../models/message'

describe 'Message', ->
  it 'creates message with collapseKey', ->
    message = new Message()
    message.collapseKey = "testeKey"
    assert.equal message.collapseKey, "testeKey"
    msg = "Message(collapseKey=testeKey)"
    assert.equal msg, message.toString()

  it 'creates message with timeToLive', ->
    message = new Message()
    message.timeToLive = 20
    assert.equal message.timeToLive, 20
    msg = "Message(timeToLive=20)"
    assert.equal message.toString(), msg

  it 'creates message with delayWhileIdle', ->
    message = new Message()
    message.delayWhileIdle = false
    assert.equal message.delayWhileIdle, false
    msg = "Message(delayWhileIdle=false)"
    assert.equal message.toString(), msg

  it 'creates message with delayWhileIdle, collapseKey and timeToLive', ->
    message = new Message()
    message.delayWhileIdle = false
    message.timeToLive = 30
    message.collapseKey = "testeKey"

    assert.equal message.delayWhileIdle, false
    msg = "Message(collapseKey=testeKey,timeToLive=30,delayWhileIdle=false)"
    assert.equal message.toString(), msg

  
  it 'creates message with data payload', ->
    message = new Message()
    message.delayWhileIdle = false
    message.timeToLive = 30
    message.collapseKey = "testeKey"
    message.restrictedPackageName = "package"
    message.addData "key1", "value1"
    message.addData "key2", "value2"
    message.addData "key3", "value3"
    message.addData "key4", "value4"

    assert.equal message.delayWhileIdle, false
    msg = "Message(collapseKey=testeKey,timeToLive=30,delayWhileIdle=false,restrictedPackageName=package,"
    msg += "data:{key1=value1,key2=value2,key3=value3,key4=value4})"
    assert.equal message.toString(), msg













