chai = sys = require 'chai'
assert = chai.assert
Result = require '../models/result'


describe 'Result', ->
  it 'has null optional parameters', ->
		result = new Result()
		assert.isNull result.messageId
		assert.isNull result.canonicalRegistrationId
		assert.isNull result.errorCode

  it 'creates result with optional parameters', ->
		result = new Result()
		result.messageId = "42" 
		result.errorCode = "D'OH" 
		result.canonicalRegistrationId = "108" 

		assert.equal "42", result.messageId
		assert.equal "D'OH", result.errorCode
		assert.equal "108", result.canonicalRegistrationId

		str = "[ messageId=42 canonicalRegistrationId=108 errorCode=D'OH ]"
		assert.equal result.toString(), str